#Information and Communications - Website
#Website Development - Year 2 Manchester Metropolitan University

The Department of Languages, Information and Communications require a website to reflect the ‘Information & Communications’ brand at MMU.

The aim is to emulate a web design company to develop a site similar to websitearchitecture.co.uk to reflect the ‘Information & Communications’ brand. This site should reflect the character the department, staff, students and resources available. It should also make the most of location, the north west new media industry and the success of our own alumni. 